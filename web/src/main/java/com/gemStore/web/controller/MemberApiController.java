package com.gemStore.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gemStore.service.UsersService;
import com.gemStore.web.bo.UsersBo;
import com.gemStore.web.converter.Impl.WebUsersConverterImpl;

@RestController
@RequestMapping("/api/member")
public class MemberApiController {
	@Autowired
	private UsersService usersService;
	@Autowired
	private WebUsersConverterImpl usersConverter;

	@RequestMapping(value = "/createMember", method = RequestMethod.POST)
	public UsersBo createMemberApi(@Valid UsersBo usersBo) {
		return usersConverter.convertToWebBo(usersService.create(usersConverter.convertToServiceBo(usersBo)));
	}

	@RequestMapping(value = "/getMember", method = RequestMethod.GET)
	public UsersBo getMember(String userName) {
		return usersConverter.convertToWebBo(usersService.findByUserName(userName));
	}
}
