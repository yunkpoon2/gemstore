package com.gemStore.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gemStore.service.Impl.TestingService;
import com.gemStore.service.bo.TestingMessageBo;

@RestController
@RequestMapping("/test")
public class TestingController {

    @Autowired
    private TestingService testingService;
    
    @RequestMapping(value = "/message", method=RequestMethod.GET)
    public TestingMessageBo message() {
        return testingService.message();
    }
}