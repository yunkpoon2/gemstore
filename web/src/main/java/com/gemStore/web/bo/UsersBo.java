package com.gemStore.web.bo;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.gemStore.deserializer.JsonDateDeserializer;
import com.gemStore.validation.UserName;

public class UsersBo {
	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;
	@Email
	private String email;
	// @NotBlank
	private String address;
	@NotNull
	private String gender;

	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonDeserialize(using = JsonDateDeserializer.class)
	private Date dob;
	@UserName
	private String userName;
	@Size(min = 8, max = 30, message = "password must in 8 - 30 characters long")
	private String password;

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static class Builder {
		private String firstName;
		private String lastName;
		private String email;
		private String address;
		private String gender;
		private Date dob;
		private String userName;
		private String password;

		public Builder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public Builder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public Builder email(String email) {
			this.email = email;
			return this;
		}

		public Builder address(String address) {
			this.address = address;
			return this;
		}

		public Builder gender(String gender) {
			this.gender = gender;
			return this;
		}

		public Builder dob(Date dob) {
			this.dob = dob;
			return this;
		}

		public Builder userName(String userName) {
			this.userName = userName;
			return this;
		}

		public Builder password(String password) {
			this.password = password;
			return this;
		}

		public UsersBo build() {
			UsersBo usersBo = new UsersBo();
			usersBo.firstName = firstName;
			usersBo.lastName = lastName;
			usersBo.email = email;
			usersBo.address = address;
			usersBo.gender = gender;
			usersBo.dob = dob;
			usersBo.userName = userName;
			usersBo.password = password;
			return usersBo;
		}
	}
}