package com.gemStore.web.converter;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

public interface Converter<E, B> {
	E convertToWebBo(B bo);

	B convertToServiceBo(E entity);

	default List<B> convertToServiceBos(final Collection<E> webBos) {
		if (CollectionUtils.isEmpty(webBos))
			return Collections.emptyList();
		return webBos.stream()
				.map(this::convertToServiceBo).collect(Collectors.toList());
    }

	default List<E> convertToWebBos(final Collection<B> webBos) {
		if (CollectionUtils.isEmpty(webBos))
			return Collections.emptyList();
		return webBos.stream()
				.map(this::convertToWebBo).collect(Collectors.toList());
    }
}