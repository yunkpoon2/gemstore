package com.gemStore.web.converter.Impl;

import org.springframework.stereotype.Component;

import com.gemStore.service.bo.UsersBo;
import com.gemStore.web.converter.Converter;

@Component
public class WebUsersConverterImpl implements Converter<com.gemStore.web.bo.UsersBo, UsersBo> {

    @Override
    public com.gemStore.web.bo.UsersBo convertToWebBo(UsersBo bo) {
    	com.gemStore.web.bo.UsersBo usersBo = new com.gemStore.web.bo.UsersBo.Builder()
        					.userName(bo.getUserName())
        					.password(bo.getPassword())
        					.dob(bo.getDob())
        					.email(bo.getEmail())
        					.gender(bo.getGender())
        					.address(bo.getAddress())
        					.firstName(bo.getFirstName())
        					.lastName(bo.getLastName())
        					.build();
        return usersBo;
    }

    @Override
    public UsersBo convertToServiceBo(com.gemStore.web.bo.UsersBo bo) {
        UsersBo userBo = new UsersBo.Builder()
				.userName(bo.getUserName())
				.password(bo.getPassword())
				.dob(bo.getDob())
				.email(bo.getEmail())
				.gender(bo.getGender())
				.address(bo.getAddress())
				.firstName(bo.getFirstName())
				.lastName(bo.getLastName())
				.build();
        return userBo;
    }
    
}