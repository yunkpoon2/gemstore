package com.gemStore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.gemStore.RepoConfig;
import com.gemStore.config.ServiceConfig;

@SpringBootApplication(scanBasePackageClasses = { ServiceConfig.class, RepoConfig.class })
@EntityScan(basePackageClasses = { RepoConfig.class })
@EnableJpaRepositories(basePackageClasses = { RepoConfig.class })
public class WebApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}
}