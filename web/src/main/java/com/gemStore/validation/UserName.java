package com.gemStore.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.gemStore.Validator.UserNameValidator;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.PARAMETER })
@Constraint(validatedBy = UserNameValidator.class)
public @interface UserName {
	String message() default "{UserName}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}