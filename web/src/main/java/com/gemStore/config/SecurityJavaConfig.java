package com.gemStore.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.gemStore.security.RestAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class SecurityJavaConfig extends WebSecurityConfigurerAdapter {
	private Logger logger = LoggerFactory.getLogger(getClass());
 
    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
 
    @Bean
    @Override
    public UserDetailsService userDetailsService() {
    	UserDetails user = User.withDefaultPasswordEncoder()
    							.username("user")
    							.password("password")
    							.roles("USER")
    							.build();
        return new InMemoryUserDetailsManager(user);
    }
 
    @Override
    protected void configure(HttpSecurity http) throws Exception { 
    	http.csrf().disable().authorizeRequests()
    	.antMatchers("/api/member/createMember").permitAll()
		.anyRequest().authenticated()
		.and().httpBasic()
        .authenticationEntryPoint(restAuthenticationEntryPoint);
    }
    
    @Bean
    public SimpleUrlAuthenticationFailureHandler myFailureHandler(){
        return new SimpleUrlAuthenticationFailureHandler();
    }
}
