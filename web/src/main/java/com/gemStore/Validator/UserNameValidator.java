package com.gemStore.Validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gemStore.service.validation.UserValidationService;
import com.gemStore.validation.UserName;

@Component
public class UserNameValidator implements ConstraintValidator<UserName, String> {

	@Autowired
	private UserValidationService userValidationService;

	@Override
	public boolean isValid(String userName, ConstraintValidatorContext context) {
		return userValidationService.isValidUserName(userName);
	}

}