package com.gemStore.service.validation;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gemStore.service.UsersService;
import com.gemStore.service.bo.UsersBo;

@Service
public class UserValidationService {

	@Autowired
	private UsersService userService;

	public boolean isValidUserName(String userName) {
		if (StringUtils.isEmpty(userName))
			return false;
		UsersBo userBo = userService.findByUserName(userName);
		if (userBo != null)
			return false;
		return true;
	}
}
