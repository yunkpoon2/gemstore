package com.gemStore.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gemStore.repo.entity.user.Users;
import com.gemStore.repo.user.UserRepository;
import com.gemStore.service.UsersService;
import com.gemStore.service.bo.UsersBo;
import com.gemStore.service.converter.Impl.ServiceUsersConverterImpl;

@Service
public class UsersServiceImpl implements UsersService {
	private UserRepository userRepository;

	private ServiceUsersConverterImpl usersConverter;

	@Autowired
	public UsersServiceImpl(UserRepository userRepository, ServiceUsersConverterImpl usersConverter) {
		this.userRepository = userRepository;
		this.usersConverter = usersConverter;
	}
	@Override
	public UsersBo create(UsersBo userBo) {
		Users user = usersConverter.convertToEntity(userBo);
		return usersConverter.convertToBo(userRepository.save(user));
	}

	@Override
	public void delete(UsersBo userBo) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<UsersBo> findAll() {
		return usersConverter.convertToBos(userRepository.findAll());
	}

	@Override
	public UsersBo findByUserName(String userName) {
		Users user = userRepository.findByUserName(userName);
		return user == null ? null : usersConverter.convertToBo(user);
	}

}
