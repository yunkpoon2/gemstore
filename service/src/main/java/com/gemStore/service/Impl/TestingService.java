package com.gemStore.service.Impl;

import org.springframework.stereotype.Service;

import com.gemStore.service.bo.TestingMessageBo;

@Service
public class TestingService{
    public TestingMessageBo message(){
        TestingMessageBo bo = new TestingMessageBo();
        bo.setMessage("testing message");
        return bo;
    }
}