package com.gemStore.service.bo;

public class TestingMessageBo{
    private String message;

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}