package com.gemStore.service.bo;

import java.util.Date;

public class UsersBo {
	private String firstName;
	private String lastName;
	private String email;
	private String address;
	private String gender;
	private Date dob;
	private String userName;
	private String password;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static class Builder {
		private String firstName;
		private String lastName;
		private String email;
		private String address;
		private String gender;
		private Date dob;
		private String userName;
		private String password;

		public Builder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public Builder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public Builder email(String email) {
			this.email = email;
			return this;
		}

		public Builder address(String address) {
			this.address = address;
			return this;
		}

		public Builder gender(String gender) {
			this.gender = gender;
			return this;
		}

		public Builder dob(Date dob) {
			this.dob = dob;
			return this;
		}

		public Builder userName(String userName) {
			this.userName = userName;
			return this;
		}

		public Builder password(String password) {
			this.password = password;
			return this;
		}

		public UsersBo build() {
			UsersBo usersBo = new UsersBo();
			usersBo.firstName = firstName;
			usersBo.lastName = lastName;
			usersBo.email = email;
			usersBo.address = address;
			usersBo.gender = gender;
			usersBo.dob = dob;
			usersBo.userName = userName;
			usersBo.password = password;
			return usersBo;
		}
	}
}
