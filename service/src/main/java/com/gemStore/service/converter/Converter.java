package com.gemStore.service.converter;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

public interface Converter<E, B> {
    E convertToEntity(B bo);

    B convertToBo(E entity);

    default List<B> convertToBos(final Collection<E> entities) {
		if (CollectionUtils.isEmpty(entities))
			return Collections.emptyList();
        return entities.stream()
                        .map(this::convertToBo)
                        .collect(Collectors.toList());
    }

    default List<E> convertToEntities(final Collection<B> bos){
		if (CollectionUtils.isEmpty(bos))
			return Collections.emptyList();
        return bos.stream()
                        .map(this::convertToEntity)
                        .collect(Collectors.toList());
    }
}