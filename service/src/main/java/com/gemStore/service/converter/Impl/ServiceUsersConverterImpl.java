package com.gemStore.service.converter.Impl;

import org.springframework.stereotype.Component;

import com.gemStore.repo.entity.user.Users;
import com.gemStore.service.bo.UsersBo;
import com.gemStore.service.converter.Converter;

@Component
public class ServiceUsersConverterImpl implements Converter<Users, UsersBo> {

    @Override
    public Users convertToEntity(UsersBo bo) {
        Users user = new Users.Builder()
        					.userName(bo.getUserName())
        					.password(bo.getPassword())
        					.dob(bo.getDob())
        					.email(bo.getEmail())
        					.gender(bo.getGender())
        					.address(bo.getAddress())
        					.firstName(bo.getFirstName())
        					.lastName(bo.getLastName())
        					.build();
        return user;
    }

    @Override
    public UsersBo convertToBo(Users entity) {
        UsersBo userBo = new UsersBo.Builder()
				.userName(entity.getUserName())
				.password(entity.getPassword())
				.dob(entity.getDob())
				.email(entity.getEmail())
				.gender(entity.getGender())
				.address(entity.getAddress())
				.firstName(entity.getFirstName())
				.lastName(entity.getLastName())
				.build();
        return userBo;
    }
    
}