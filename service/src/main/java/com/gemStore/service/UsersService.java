package com.gemStore.service;

import java.util.List;

import com.gemStore.service.bo.UsersBo;

public interface UsersService{
	public UsersBo create(UsersBo userBo);

    public void delete(UsersBo userBo);

    public List<UsersBo> findAll();

    public UsersBo findByUserName(String userName);

}
