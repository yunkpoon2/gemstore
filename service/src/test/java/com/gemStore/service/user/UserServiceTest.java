package com.gemStore.service.user;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.util.CollectionUtils;

import com.gemStore.repo.entity.user.Users;
import com.gemStore.repo.user.UserRepository;
import com.gemStore.service.UsersService;
import com.gemStore.service.Impl.UsersServiceImpl;
import com.gemStore.service.bo.UsersBo;
import com.gemStore.service.converter.Impl.ServiceUsersConverterImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	private Logger logger = Logger.getLogger(getClass());
	@Mock
	private UserRepository userRepository;

	private ServiceUsersConverterImpl userConverter;

	private UsersService userService;

	@Before
	public void setup() {
		userConverter = new ServiceUsersConverterImpl();
		userService = new UsersServiceImpl(userRepository, userConverter);
	}

	@Test
	public void testFindAllData_Null() {
		Mockito.when(userRepository.findAll()).thenReturn(null);
		List<UsersBo> userBoList = userService.findAll();
		assertTrue(CollectionUtils.isEmpty(userBoList));

	}

	@Test
	public void testFindAllData_List() {
		List<Users> userList = new ArrayList<Users>();
		Users user = new Users.Builder().userName("Testing").build();
		logger.info(userList.toString());
		Mockito.when(userRepository.findAll()).thenReturn(Collections.singletonList(user));
		List<UsersBo> userBoList = userService.findAll();
		logger.info(userBoList.toString());
		assertFalse(CollectionUtils.isEmpty(userBoList));
	}

	@Test
	public void testCreate() {
		UsersBo bo = new UsersBo.Builder().userName("Testing").build();
		Mockito.when(userRepository.save(Mockito.any(Users.class))).thenReturn(userConverter.convertToEntity(bo));
		assertTrue(StringUtils.equals(userService.create(bo).getUserName(), bo.getUserName()));
	}
}
