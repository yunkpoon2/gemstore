package com.gemStore.repo.user;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;

import org.jboss.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.gemStore.repo.entity.user.UserRole;
import com.gemStore.repo.entity.user.Users;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRoleRepositoryTest {
	private Logger logger = Logger.getLogger(getClass());
    @Autowired
    private TestEntityManager entityManager;
    
    @Autowired
    private UserRoleRepository userRoleRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Before
    public void before(){
    	Users user = new Users.Builder()
				.userName("TestingUser")
				.password("password")
				.gender("M")
				.firstName("John")
				.lastName("Wick")
				.address("Testing\\nTesting2\\nHK")
				.email("john.wick@gmail.com")
				.build();

		UserRole userRole = new UserRole.Builder().role("Admin").build();

		user.setUserRole(Collections.singleton(userRole));
		userRole.setUsers(Collections.singleton(user));

		entityManager.persist(user);
		entityManager.flush();
	}
    
    @Test
    public void testSave(){
        UserRole userRole = new UserRole.Builder()
										.role("Admin")
										.build();
        userRole = userRoleRepository.save(userRole);
        logger.info("The assigned user Role id is " + userRole.getRoleId());
        assertNotNull(userRole.getRoleId());
    }
    
    @Test
    public void testFindAll(){
        List<UserRole> userRoleList = userRoleRepository.findAll();
        userRoleList.stream().forEach(logger::info);
        assertTrue(!CollectionUtils.isEmpty(userRoleList));
    }
    
    @Test
    public void testFindByRole(){
        UserRole userRole = userRoleRepository.findByRole("Admin");
        logger.info(userRole.toString());
        assertNotNull(userRole.getRoleId());
    }
    
    @Test
    public void testFindByRoleNotExist(){
        UserRole userRole = userRoleRepository.findByRole("NotExistRole");
        assertNull(userRole);
    }
    
    @Test
    public void testDeleteByRole(){
    	UserRole userRole = userRoleRepository.findByRole("Admin");
//    	userRole.getUsers().stream().forEach(user -> user.getUserRole().remove(this));
    	userRoleRepository.delete(userRole);
    	for (Users user : userRole.getUsers()) {
    		user.getUserRole().remove(userRole);
    	}
    	
    	userRole = userRoleRepository.findByRole("Admin");
    	Users users = userRepository.findByUserName("TestingUser");
    	assertNull(userRole);
    	assertNotNull(users);
    }
}
