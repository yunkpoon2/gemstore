package com.gemStore.repo.user;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;

import org.jboss.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.gemStore.repo.entity.user.UserRole;
import com.gemStore.repo.entity.user.UserRoleMapping;
import com.gemStore.repo.entity.user.Users;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
    private Logger logger = Logger.getLogger(getClass());
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserRoleRepository userRoleRepository;
    
    @Autowired
    private UserRoleMappingRepository userRoleMappingRepository;

    @Before
    public void before(){
        Users user = new Users.Builder()
        					.userName("TestingUser")
        					.password("password")
        					.gender("M")
        					.firstName("John")
        					.lastName("Wick")
        					.address("Testing\\nTesting2\\nHK")
        					.email("john.wick@gmail.com")
        					.build();

        UserRole userRole = new UserRole.Builder()
        								.role("Admin")
        								.build();
        
        user.setUserRole(Collections.singleton(userRole));
        userRole.setUsers(Collections.singleton(user));
        
        entityManager.persist(user);
        entityManager.flush();
    }
    
    @Test
    public void testSave(){
        Users user = new Users();
        user.setUserName("TestingUser1");
        user.setPassword("password");
        UserRole userRole = new UserRole.Builder()
										.role("Admin")
										.build();
        user.setUserRole(Collections.singleton(userRole));
        logger.info("The original user id is " + user.getUserId());
        user = userRepository.save(user);
        logger.info("The assigned user id is " + user.getUserId());
        logger.info("The user role id is " + user.getUserRole().stream().findFirst().orElse(null));
        assertNotNull(user.getUserId());
        assertNotNull(user.getUserRole().stream().findFirst().orElse(null));
    }

    @Test
    public void testFindAll(){
        List<Users> userList = userRepository.findAll();
        userList.stream().forEach(logger::info);
        assertTrue(!CollectionUtils.isEmpty(userList));
    }

    @Test
    public void testFindByUser(){
        Users user = userRepository.findByUserName("TestingUser");
        logger.info(user.toString());
        assertNotNull(user.getUserId());
        assertTrue(!CollectionUtils.isEmpty(user.getUserRole()));
    }
    
    @Test
    public void testCascadeInRoleMapping() {
    	Users user = userRepository.findByUserName("TestingUser");
    	userRepository.delete(user);
    	UserRoleMapping userRoleMapping = userRoleMappingRepository.findByUsers(user);
    	UserRole userRole = userRoleRepository.findByRole("Admin");
    	assertNull(userRoleMapping);
    	assertNotNull(userRole);
    	
    }
} 
