package com.gemStore.repo.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gemStore.repo.entity.user.Users;

public interface UserRepository extends JpaRepository<Users, Long>{
    public Users findByUserName(String userName);

    public Users save(Users user);

    public void delete(Users user);

	public List<Users> findAll();

    //public User findById(int id);

    //public void update(User user);

}
