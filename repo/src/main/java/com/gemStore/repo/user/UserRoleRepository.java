package com.gemStore.repo.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gemStore.repo.entity.user.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long>{
    public UserRole findByRole(String role);

    public UserRole save(UserRole userRole);

    public void delete(UserRole user);

	public List<UserRole> findAll();

}