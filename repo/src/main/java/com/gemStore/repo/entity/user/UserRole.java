package com.gemStore.repo.entity.user;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users_role")
public class UserRole {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "role_id")
	private Long roleId;

	@Column(name = "role")
	private String role;

	@Column(name = "role_desc")
	private String roleDesc;

	@OneToMany(mappedBy = "userRole", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<UserRoleMapping> userRoleMapping;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public List<UserRoleMapping> getUserRoleMapping() {
		return userRoleMapping;
	}

	public void setUserRoleMapping(List<UserRoleMapping> userRoleMapping) {
		this.userRoleMapping = userRoleMapping;
	}

	public static class Builder {
		private Long roleId;
		private String role;
		private String roleDesc;
		private List<UserRoleMapping> userRoleMapping;

		public Builder roleId(Long roleId) {
			this.roleId = roleId;
			return this;
		}

		public Builder role(String role) {
			this.role = role;
			return this;
		}

		public Builder roleDesc(String roleDesc) {
			this.roleDesc = roleDesc;
			return this;
		}

		public Builder userRoleMapping(List<UserRoleMapping> userRoleMapping) {
			this.userRoleMapping = userRoleMapping;
			return this;
		}

		public UserRole build() {
			UserRole userRole = new UserRole();
			userRole.roleId = roleId;
			userRole.role = role;
			userRole.roleDesc = roleDesc;
			userRole.userRoleMapping = userRoleMapping;
			return userRole;
		}
	}
}
