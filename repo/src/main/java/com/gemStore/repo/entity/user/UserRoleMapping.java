package com.gemStore.repo.entity.user;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_role_mapping")
public class UserRoleMapping {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_role_mapping_id", updatable = false, nullable = false)
	private Long userRoleMappingId;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Users users;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private UserRole userRole;

	public Long getUserRoleMappingId() {
		return userRoleMappingId;
	}

	public void setUserRoleMappingId(Long userRoleMappingId) {
		this.userRoleMappingId = userRoleMappingId;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public static class Builder {
		private Long userRoleMappingId;
		private Users users;
		private UserRole userRole;

		public Builder userRoleMappingId(Long userRoleMappingId) {
			this.userRoleMappingId = userRoleMappingId;
			return this;
		}

		public Builder users(Users users) {
			this.users = users;
			return this;
		}

		public Builder userRole(UserRole userRole) {
			this.userRole = userRole;
			return this;
		}

		public UserRoleMapping build() {
			UserRoleMapping userRoleMapping = new UserRoleMapping();
			userRoleMapping.userRoleMappingId = userRoleMappingId;
			userRoleMapping.users = users;
			userRoleMapping.userRole = userRole;
			return userRoleMapping;
		}
	}
}
