package com.gemStore;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.gemStore.repo")
public class RepoConfig {
}
