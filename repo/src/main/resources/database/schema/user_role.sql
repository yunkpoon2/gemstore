DROP TABLE IF EXISTS user_role;
CREATE TABLE user_role (
    user_role_id SMALLINT unsigned NOT NULL auto_increment,
    role VARCHAR(30) NOT NULL,
    role_desc VARCHAR(30),
    CONSTRAINT user_role_pk PRIMARY KEY (user_role_id)
);