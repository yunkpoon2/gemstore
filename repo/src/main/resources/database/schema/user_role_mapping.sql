DROP TABLE IF EXISTS user_role_mapping;
CREATE TABLE user_role_mapping (
    user_role_mapping_id SMALLINT unsigned NOT NULL auto_increment,
    user_id SMALLINT unsigned NOT NULL,
    role_id SMALLINT unsigned NOT NULL,
    CONSTRAINT user_role_mapping_pk PRIMARY KEY (user_role_mapping_id)
);